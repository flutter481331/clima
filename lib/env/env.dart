import 'package:envied/envied.dart';

part 'env.g.dart';

@Envied(path: '.env')
abstract class Env {
  @EnviedField(varName: 'OPEN_WEATHER_MAP_API_KEY')
  static const String kOpenWeatherAppApiKey = _Env.kOpenWeatherAppApiKey;

  @EnviedField(varName: 'OPEN_WEATHER_MAP_BASE_URL')
  static const String kOpenWeatherAppBaseUrl = _Env.kOpenWeatherAppBaseUrl;
}
