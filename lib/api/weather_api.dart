import 'dart:convert';
import 'package:http/http.dart';
import '../env/env.dart';
import '../utilities/constants.dart';
import 'package:clima/services/location.dart';

class WeatherAPI {
  final String weatherEndpoint = '/weather';

  Future getWeatherByCityName(cityName) async {
    String urlParam =
        '?q=$cityName&appid=${Env.kOpenWeatherAppApiKey}&units=metric';
    final String urlComplete =
        Env.kOpenWeatherAppBaseUrl + weatherEndpoint + urlParam;
    Uri url = Uri.parse(urlComplete);

    try {
      Response response = await get(url);
      if (response.statusCode == kRespSuccess) {
        return jsonDecode(response.body);
      } else {
        print(response.body);
      }
    } catch (e) {
      print('Error getting weather' + e.toString());
    }
    return '';
  }

  Future getWeather() async {
    Location location = Location();
    await location.getCurrentLocation();
    double lon = location.longitude;
    double lat = location.latitude;

    String urlParam = '?lat=$lat&lon=$lon&appid=' +
        Env.kOpenWeatherAppApiKey +
        '&units=metric';
    final String urlComplete =
        Env.kOpenWeatherAppBaseUrl + weatherEndpoint + urlParam;
    print('URL: ' + urlComplete);

    Uri url = Uri.parse(urlComplete);

    try {
      Response response = await get(url);
      if (response.statusCode == kRespSuccess) {
        return jsonDecode(response.body);
      } else {
        print(response.body);
      }
    } catch (e) {
      print('Error getting weather' + e.toString());
    }
    return '';
  }
}
